import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ProductosService } from '../productos.service';
import { productos } from '../productos.model';

@Component({
  selector: 'app-detalles',
  templateUrl: './detalles.page.html',
  styleUrls: ['./detalles.page.scss'],
})
export class DetallesPage implements OnInit {
  producto: productos;
  constructor(
    private activeRouter: ActivatedRoute,
    private ProductosService: ProductosService,
    private router: Router,
    private alertController: AlertController
  ) { }

  ngOnInit() {

    this.activeRouter.paramMap.subscribe(
      paramMap => {
        if(!paramMap.has('productoId')){
          return;
        }
        const productoId =  parseInt( paramMap.get('productoId'));
        this.producto = this.ProductosService.obtenerProducto(productoId);
        console.log(productoId);
      }
    );
  }

  borrarProducto(){
    this.alertController.create({
      header: "Borrar producto",
      message: "Esta seguro que desea borrar este producto?",
      buttons:[
        {
          text:"No",
          role: 'no'
        },
        {
          text: 'Borrar',
          handler: () => {
            this.ProductosService.borrarProducto(this.producto.codigo);
            this.router.navigate(['./productos']);
          }
        } 
      ]
    })
    .then(
      alertEl => {
        alertEl.present();
      }
    );
    
  }

  actualizar(codigo: number) {
    this.router.navigate(["/productos/editar/" + codigo]);
  }
}

