import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from "@angular/router";
import { ProductosService } from "../productos.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { productos } from '../productos.model';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  producto: productos;
  formEditarProducto: FormGroup;

  constructor(
    private activeRouter: ActivatedRoute,
    private serviceProducto: ProductosService,
    private router: Router
  ) { }

  ngOnInit() {

  this.activeRouter.paramMap.subscribe(paramMap => {
      if (!paramMap.has('productoId')) {
        return;
      }
      const productoId = parseInt(paramMap.get('productoId'));
      this.producto = this.serviceProducto.obtenerProducto(productoId);

    });

    this.formEditarProducto = new FormGroup({
      pcodigo: new FormControl(this.producto.codigo, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pnombre: new FormControl(this.producto.nombre, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pcantidad: new FormControl(this.producto.cantidad, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pprecio: new FormControl(this.producto.precio, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescripcion: new FormControl(this.producto.descripcion, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),
    });

    this.formEditarProducto.value.pnombre = this.producto.nombre;

  }

  editarProducto() {

    if (!this.formEditarProducto.valid) {
      return;
    }

    this.serviceProducto.editarProducto(
      this.formEditarProducto.value.pcodigo,
      this.formEditarProducto.value.pnombre,
      this.formEditarProducto.value.pcantidad,
      this.formEditarProducto.value.pprecio,
      this.formEditarProducto.value.pdescripcion 
    );

    this.formEditarProducto.reset();
    
    this.router.navigate(['./productos']);
    
  }
}


