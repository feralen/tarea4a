import { Component, Input, OnInit } from "@angular/core";
import { ProductosService } from "./productos.service";
import { productos } from "./productos.model";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
})
export class ProductosPage implements OnInit {

  @Input() author: string;
  producto: productos[];
  constructor(
    private ProductosServices: ProductosService,
    private router: Router,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    console.log("Carga inicial");
    this.producto = this.ProductosServices.getAll();
  }

  ionViewWillEnter() {
    console.log("Se obtuvo la lista");
    this.producto = this.ProductosServices.getAll();
  }

  detalles(codigo: number) {
    this.router.navigate(["/productos/detalles/" + codigo]);
  }

  borrar(codigo: number) {
    this.alertController
      .create({
        header: "Borrar producto",
        message: "Esta seguro que desea borrar este producto?",
        buttons: [
          {
            text: "No",
            role: "no",
          },
          {
            text: "Borrar",
            handler: () => {
              this.ProductosServices.borrarProducto(codigo);
              this.producto = this.ProductosServices.getAll();
            },
          },
        ],
      })
      .then((alertEl) => {
        alertEl.present();
      });
  }

  actualizar(codigo: number) {
    this.router.navigate(["/productos/editar/" + codigo]);
  }

}

