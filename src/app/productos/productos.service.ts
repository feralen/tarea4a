import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { productos } from "./productos.model";

@Injectable({
  providedIn: "root",
})
export class ProductosService {
  private productos: productos[] = [
    {
      codigo: 1111,
      nombre: "Carro Electrico",
      cantidad: 1,
      precio: 25000,
      descripcion: "Carro electrico a control remoto con batería recargable"
    },
  ];
  constructor(
    private http: HttpClient
  ) {}

  getAll() {
    return [...this.productos];
  }

  obtenerProducto(productoId: number) {
    return {
      ...this.productos.find((producto) => {
        return producto.codigo === productoId;
      }),
    };
  }

  borrarProducto(productoId: number) {
    this.productos = this.productos.filter((producto) => {
      return producto.codigo !== productoId;
    });
  }

  anadirProducto(
    pcodigo: number,
    pnombre: string,
    pcantidad: number,
    pprecio: number,
    pdescripcion: string,    
  ) {
    const producto: productos = {
      codigo: pcodigo,
      nombre: pnombre,
      cantidad: pcantidad,
      precio: pprecio,
      descripcion: pdescripcion,
    }
    this.http.post('https://tarea4-6c159.firebaseio.com/anadirProducto.json',
       { 
          ...producto, 
          id:null
        }).subscribe(() => {
          console.log('entro');
        });

    this.productos.push(producto);
  }

  editarProducto(
    pcodigo: number,
    pnombre: string,
    pcantidad: number,
    pprecio: number,
    pdescripcion: string,  
  ) {
    let index = this.productos.map((x) => x.codigo).indexOf(pcodigo);

    this.productos[index].codigo = this.productos.map((x) => x.codigo).indexOf(pcodigo);
    this.productos[index].nombre = pnombre;
    this.productos[index].cantidad = pcantidad;
    this.productos[index].precio = pprecio;
    this.productos[index].descripcion = pdescripcion;

    console.log(this.productos);
  }

}
