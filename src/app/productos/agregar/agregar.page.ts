import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { ProductosService } from "../productos.service";

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  formAnadirProducto: FormGroup;

  constructor(private serviceProductos: ProductosService, private router: Router) { }

  ngOnInit() {
    this.formAnadirProducto = new FormGroup({
      pcodigo: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(1)],
      }),
      pnombre: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pcantidad: new FormControl(1, {
        updateOn: "blur",
        validators: [Validators.required, Validators.min(1)],
      }),
      pprecio: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      pdescripcion: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required, Validators.minLength(20)],
      }),     
    });
  }

  anadirProducto() {

    if (!this.formAnadirProducto.valid) {
      return;
    }

    this.serviceProductos.anadirProducto(
      this.formAnadirProducto.value.pcodigo,
      this.formAnadirProducto.value.pnombre,
      this.formAnadirProducto.value.pcantidad,
      this.formAnadirProducto.value.pprecio,
      this.formAnadirProducto.value.pdescripcion

    );

    this.formAnadirProducto.reset();
    
    this.router.navigate(["/productos"]);
    
  }
}

