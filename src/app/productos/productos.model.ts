export interface productos{
    codigo: number;
    nombre: string;
    cantidad: number;
    precio: number;
    descripcion: string;
}