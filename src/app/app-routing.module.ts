import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'productos',
    children: [
      {
        path: "",
        loadChildren: "./productos/productos.module#ProductosPageModule",
      },
      {
        path: "add",
        loadChildren: "./productos/agregar/agregar.module#AgregarPageModule",
      },
      {
        path: "detalles/:productoId",
        loadChildren: "./productos/detalles/detalles.module#DetallesPageModule",
      },
      {
        path: "editar/:productoId",
        loadChildren: "./productos/editar/editar.module#EditarPageModule",
      }
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
